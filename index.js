#!/usr/bin/env node

const keychain = require("keychain");
const axios = require("axios");
const inquirer = require("inquirer");
const opn = require("opn");
const ora = require("ora");
const fs = require("fs");
const { promisify } = require("util");

writeFile = promisify(fs.writeFile);
readFile = promisify(fs.readFile);
getPassword = promisify(keychain.getPassword);

inquirer.registerPrompt(
  "autocomplete",
  require("inquirer-autocomplete-prompt")
);

async function readUsernameFromFile() {
  try {
    const buf = await readFile("./user.json");
    const json = JSON.parse(buf.toString());
    return json.name;
  } catch (error) {
    return false;
  }
}

async function writeUsernameToFile(name) {
  try {
    await writeFile("./user.json", JSON.stringify({ name }));
  } catch (error) {
    throw error;
  }
}

async function wipePass() {
	await writeFile('./user.json', '');
}


function getCredentials() {
  return new Promise(async (resolve, reject) => {
    try {
      let username;

      const existing = await readUsernameFromFile();

      if (!existing) {
        const ans = await inquirer.prompt([
          {
            type: "input",
            message: "Jira Username:",
            name: "username"
          }
        ]);
        username = ans.username;

        await writeUsernameToFile(username);
      } else {
        username = existing;
      }

      keychain.getPassword(
        { account: username, service: "JiraAccount" },
        async (err, pass) => {
          if (err) {
						await wipePass();
						return console.log(err.message);
					}
          resolve({ pass, username });
        }
      );
    } catch (error) {
			throw error;
    }
  });
}

async function main() {
  const arg = process.argv[2];

  const { pass, username } = await getCredentials();

  const encode = msg => encodeURIComponent(msg);
  const getIssuesBySummary = msg =>
    axios.get(
      `https://${username}:${pass}@jira.rmn.com/rest/api/2/issue/picker?query=${encode(
        msg
      )}`
    );

  async function getIssuesAsStrings(input = arg) {
    if (!input) return [];
    const { data } = await getIssuesBySummary(input);
    const { sections } = data;
    const { issues } = sections[0];
    const issueStrings = issues.map(iss => `${iss.key} - ${iss.summaryText}`);
    return issueStrings;
  }

  async function prompt() {
    const { issue } = await inquirer.prompt([
      {
        type: "autocomplete",
        name: "issue",
        message: "Search for an issue:",
        source(ansSoFar, input) {
          return getIssuesAsStrings(input);
        }
      }
    ]);
    const iss = issue.split(" ")[0];
    opn(`https://jira.rmn.com/browse/${iss}`);
    process.exit();
  }

  prompt();
}

main();
